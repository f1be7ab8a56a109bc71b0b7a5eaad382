#include <stdio.h>

typedef struct {
  int size;
  int *segments;
} vers;

#define makeversion(...) \
  ({ static int x[] = { __VA_ARGS__ }; (vers) { sizeof x/sizeof *x, x }; })

size_t verstostr(vers v, char *buf, size_t size) {
  size_t ret = snprintf(0, 0, "%d", v.segments[0]);
  for (int i = 1; i < v.size; i++)
    ret += snprintf(0, 0, ".%d", v.segments[i]);

  size_t tmp = snprintf(buf, size, "%d", v.segments[0]);
  for (int i = 1; i < v.size && size >= tmp; i++) {
    size -= tmp;
    buf += tmp;
    tmp = snprintf(buf, size, ".%d", v.segments[i]);
  }

  return ret;
}

int verscomp(vers v1, vers v2) {
  int smaller = v1.size < v2.size ? v1.size : v2.size;
  for (int i = 0; i < smaller; i++)
         if (v1.segments[i] > v2.segments[i]) return  1;
    else if (v1.segments[i] < v2.segments[i]) return -1;

  if (v1.size == v2.size) return  0;
  if (smaller == v1.size) return -1;
  return 1;
}

int main() {
  vers v[] = {
    makeversion(1, 2, 3),
    makeversion(1, 3, 5),
    makeversion(1, 3, 5, 7, 3)
  };

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      char str[2][10];
      verstostr(v[i], str[1], sizeof str[1]);
      verstostr(v[j], str[2], sizeof str[2]);

      int comp = verscomp(v[i], v[j]);

           if (comp > 0) printf("%9s > %9s\n", str[1], str[2]);
      else if (comp < 0) printf("%9s < %9s\n", str[1], str[2]);
      else               printf("%9s = %9s\n", str[1], str[2]);
    }
  }


}
